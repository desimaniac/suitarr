ARG REGISTRY_IMAGE
FROM ${REGISTRY_IMAGE}:base
LABEL app="nanomon"

ARG DEBIAN_FRONTEND="noninteractive"

ENV APP="Nanomon"
EXPOSE 8081
HEALTHCHECK --interval=60s CMD curl -fsSL http://localhost:8081 || exit 1

# install packages
RUN apt update && \
    apt install -y --no-install-recommends --no-install-suggests \
        apache2 php libapache2-mod-php php-curl && \
# clean up
    apt autoremove -y && \
    apt clean && \
    rm -rf /tmp/* /var/lib/apt/lists/* /var/tmp/*

# install app
# https://github.com/NanoTools/nanoNodeMonitor/releases
RUN url="https://github.com/NanoTools/nanoNodeMonitor/archive/v1.4.14.tar.gz" && \
    curl -fsSL "${url}" | tar xzf - -C "${APP_DIR}" --strip-components=1 && \
    chmod -R u=rwX,go=rX "${APP_DIR}"

COPY nanomon/ /
