ARG REGISTRY_IMAGE
FROM ${REGISTRY_IMAGE}:base
LABEL app="nanode"

ARG DEBIAN_FRONTEND="noninteractive"

ENV APP="Nanode"
EXPOSE 7075/tcp 7075/udp 7076/tcp

# install app
# https://github.com/nanocurrency/raiblocks/releases
RUN url="https://github.com/nanocurrency/nano-node/releases/download/V17.1/nano-17.1-Linux.tar.bz2" && \
    curl -fsSL "${url}" | tar xjf - -C "${APP_DIR}" --strip-components=1 && \
    chmod -R u=rwX,go=rX "${APP_DIR}"

COPY nanode/ /
