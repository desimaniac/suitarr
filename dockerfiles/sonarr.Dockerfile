ARG REGISTRY_IMAGE
FROM ${REGISTRY_IMAGE}:mono
LABEL app="sonarr"

ARG DEBIAN_FRONTEND="noninteractive"

ENV APP="Sonarr"
EXPOSE 8989
HEALTHCHECK --interval=60s CMD curl -fsSL http://localhost:8989 || exit 1

# install app
# https://download.sonarr.tv/v2/master/mono/
RUN url="https://download.sonarr.tv/v2/master/mono/NzbDrone.master.2.0.0.5301.mono.tar.gz" && \
    curl -fsSL "${url}" | tar xzf - -C "${APP_DIR}" --strip-components=1 && \
    chmod -R u=rwX,go=rX "${APP_DIR}"

COPY sonarr/ /
