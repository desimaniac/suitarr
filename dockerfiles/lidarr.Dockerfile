ARG REGISTRY_IMAGE
FROM ${REGISTRY_IMAGE}:mono
LABEL app="lidarr"

ARG DEBIAN_FRONTEND="noninteractive"

ENV APP="Lidarr"
EXPOSE 8686
HEALTHCHECK --interval=60s CMD curl -fsSL http://localhost:8686 || exit 1

# install app
# https://github.com/lidarr/Lidarr/releases
RUN url="https://github.com/lidarr/Lidarr/releases/download/v0.5.0.583/Lidarr.develop.0.5.0.583.linux.tar.gz" && \
    curl -fsSL "${url}" | tar xzf - -C "${APP_DIR}" --strip-components=1 && \
    chmod -R u=rwX,go=rX "${APP_DIR}"

COPY lidarr/ /
